// Практичне завдання.
// Реалізувати функцію підсвічування клавіш.

// Технічні вимоги:

// - У файлі index.html лежить розмітка для кнопок.
// - Кожна кнопка містить назву клавіші на клавіатурі
// - Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
// Але якщо при натискані на кнопку її  не існує в розмітці, то попередня активна кнопка повина стати неактивною.

 
const btns = document.querySelectorAll('.btn'); 

document.addEventListener('keydown', (e) => {


    const key = e.key.toLowerCase();

    console.log(key)

    btns.forEach((btn, index) => {
        console.log(index);

        if(btn.textContent.toLowerCase() === key) {
            btn.style.backgroundColor = 'blue';

        } else if (btn.textContent.toLowerCase() !== key) {
            btn.style.backgroundColor = 'black';
        }

    })
})